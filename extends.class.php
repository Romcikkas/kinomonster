<?php

interface Say {
	public function say();

}

abstract class Human implements Say {
	private $name;

	public function __construct($name) {
		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}
}

class Man extends Human {

	public function __construct($name) {
		parent::__construct($name);
	} 

	public function beard() {
		echo "u menja rastet boroda";
	}

public function say() {
	echo "U menja muzskoj golas, menia zovut ".$this->getName()." i ";
}

}

class Woman extends Human {

	public function __construct($name) {
		parent::__construct($name);
	}
	public function bearChildren() {
		echo "ja rozhaju detej";
	}

	public function say() {
		echo "U menia zhenskij golos, menia zovut ".$this->getName()." i ";
	}

}

$man = new Man("Roman");
$man->say();
$man->beard();

echo "<br>";

$woman = new Woman("Marija");
$woman->say();
$woman->bearChildren();




?>